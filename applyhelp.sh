#! /usr/bin/bash
# PowerGCC patch applier

# This file presumes your Binutils is at ../binutils-gdb
# your GCC is at ../gcc. Update it accordingly and
# the repository is at repo folder.

# Be sure to have a SYSROOT variable pointing to
# where the Lockdown system root is (usually POWER/iso/root)
#
# This script won't use it but Lockdown will.

BINUTILS=binutils-gdb
GCC=gcc
THIS=`pwd`

cd $GCC
patch -p1 < $THIS/gcc.patch
cp $THIS/lockdown.h gcc/config/

cd $THIS

cd $BINUTILS

patch -p1 < $THIS/binutils.patch
cp $THIS/elf_x86_64_lockdown.sh ld/emulparams

cd ld
automake

echo Everything set! You may now configure Binutils and GCC.

cd $THIS
