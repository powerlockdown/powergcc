#! /usr/bin/bash
# Fetch Autoconf 2.69 and Automake 1.15.1

THIS=`pwd`
ACM_BUILD=acm-build

AUTOCONF_DIR=autoconf-2.69
AUTOMAKE_DIR=automake-1.15.1

AUTOCONF_NAME=$AUTOCONF_DIR.tar.xz
AUTOMAKE_NAME=$AUTOMAKE_DIR.tar.xz

AUTOCONF_URL=https://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz
AUTOMAKE_URL=https://ftp.gnu.org/gnu/automake/automake-1.15.1.tar.xz

if [ ! -e $AUTOCONF_NAME ]
then
	wget $AUTOCONF_URL
fi

if [ ! -e $AUTOMAKE_NAME ]
then
	wget $AUTOMAKE_URL
fi

tar -xf $AUTOCONF_NAME
tar -xf $AUTOMAKE_NAME

cd $AUTOCONF_DIR/
./configure --prefix=$THIS/$ACM_BUILD
make all && make install

echo \*\*\* fetchacmake: FINISHED BUILDING AUTOCONF, NOW BUILDING AUTOMAKE

cd $THIS/$AUTOMAKE_DIR
./configure --prefix=$THIS/$ACM_BUILD
make all && make install

echo \*\*\* fetchacmake: FINISHED BUILDING AUTOMAKE 

cd $THIS



